<?php

// if (!class_exists('WP_List_Table')) {
//     require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
// }


 class Master_Applications_Lists extends WP_List_Table
{

    /** Class constructor */
    public function __construct()
    {

        parent::__construct([
            'singular' => __('Proposed Product Dates', 'sp'), //singular name of the listed records
            'plural' => __('Proposed Product Dates', 'sp'), //plural name of the listed records
          //  'ajax' => false //does this table support ajax?
        ]);

    }
}


class Proposed_Products_Date
{

    // class instance
    static $instance;

    // class constructor
    public function __construct()
    {
      //  add_filter('set-screen-option', [__CLASS__, 'set_screen'], 10, 3);
        add_action('admin_menu', [$this, 'plugin_menu']);
    }

    public function plugin_menu()
    {

        $hook = add_menu_page(
            'Proposed Product Dates',
            'Proposed Product Dates',
            'manage_options',
            'proposed_product_dates',
            [$this, 'plugin_setting_page']
        );
    }

    public function plugin_setting_page()
    {

        global $product_id;
        global $saleprice;
        $ids = explode(',', $_GET['ids']);
        $updatedIDs = $_GET['ids'];


        if (empty($_GET['ids'])) {      
            echo 'Please Select Record(s) First';
        } else {
            global $wpdb;
           // $ids = explode(',', $_GET['ids']);
            // $ids = $_GET['ids'];
            // print_r($ids);
            
            //$formURL = admin_url('admin.php?page=discounted_deals'); 
            ?>
     <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>    
    
          
    
            <form method="post" action="" id="formId">
                <table class="tablepress dataTable" id="master_applications_table">
                    <thead>
                    <tr>
                        <th>
                            <center>Product ID</center>
                        </th>
                        <th>
                            <center>Product Name</center>
                        </th>
                  
                       
    
    
                    </tr>
                    </thead>
    
                    <?php
                    // $anyProposedInterview = 0;
                    foreach ($ids as $id) {
                        $entries_count = $wpdb->get_results ("SELECT * FROM `wp_posts` WHERE `post_type`= 'product' AND ID = $id " );
                     
                         foreach($entries_count as $entry)
                            ?>
                            <tr>
                                <td>
                                    <center><?php echo $entry->ID; ?></center>
                                </td>
                                <td>
                                    <center><?php echo $entry->post_title; 
                                   
                                    ?></center>
                                </td>
                               
                             
                            </tr>
                          
                            <input type="hidden" name="products_id" value="<?php echo $entry->ID ?>"/>
                           
                        <?php 
                        //}
                    }
                  
                    ?>
                    <br>
                    <h1>Weekly Deals</h1>
                    <br>
                    <label style= "font-weight: bold; margin-right: 5px;">Start Date: </label><input id = "pickStartDate" type="text" name="start-date" placeholder="Pick a date" required/>
                    <label style= "font-weight: bold; margin-right: 5px; margin-left: 15px;">End Date: </label><input id = "pickEndDate" type="text" name="end-date" placeholder="Pick a date" required/>
                    <label style= "font-weight: bold; margin-right: 5px; margin-left: 15px;">Sale Price: </label><input id = "saleprice" type="number" name="sale-price" placeholder="Sale Price.." required/>
                    <input style = "background-color: #0073aa;
                                    font-size: 18px;
                                    font-family: serif;
                                    color: white;
                                    padding-left: 30px;
                                    padding-right: 30px;
                                    padding-bottom: 4px;
                           
                                    border: 1px solid #0073aa;
                                    border-radius: 5px;
                                    cursor: pointer;
                                    padding-top: 4px;
                                    margin-left: 10px" type = "submit" name="submit-deals"  class="btn btn-info btn-sm" value="Submit" > 
                                <br><br><br>
                </table>
                <?php 
                if(!empty($_POST)) { // form has been submitted 
 
 if (isset($_POST["submit-deals"])){ 
     global $wpdb;
                // collect value of input field
  $pickStartDate = $_POST['start-date']; 
  $saleprice = $_POST['sale-price']; 
  
   $pickEndDate = $_POST['end-date']; 
   $updatedPickStartDate = date("Y-m-d", strtotime($pickStartDate));
   $updatedPickEndDate = date("Y-m-d", strtotime($pickEndDate));

   $daterange = $updatedPickStartDate.' - '.$updatedPickEndDate;
//    echo $daterange;

   //$week = strtotime(date("Y-m-d",$daterange));
   $upcoming_week =date("m/d/Y",$daterange);

   

$trimmed = str_replace('-', '1', $daterange) ;
$upcoming_week =date("m/d/Y",$trimmed);
// echo '$trimmed--->'.$trimmed ;

  
//    $time = strtotime($daterange);

//    $newformat = date('Y-m-d',$time);
//    echo gettype($daterange)."\n";
//    echo gettype($upcoming_week)."\n";
//    echo gettype($newformat)."\n";
   
   

// echo gettype($trimmed)."\n";
   
//    echo '$pickStartDate--->'.$pickStartDate;
//    echo '$pickEndDate--->'.$pickEndDate;
//    echo '$$daterange--->'.$daterange;


// $entries_count = $wpdb->get_row ("SELECT * FROM `wp_post` WHERE `post_type`= 'product'" );

   //echo '$products_id--->'.$ids;
   foreach($ids as $id){
    // echo '$products_id--->'.$id;
    $product = new WC_Product( $id );

    $originalDate = $_POST['datepicker'];

   // SQL query $sql = "SELECT * FROM checkdate WHERE DATE(createdat) = '$newDate'"; 

//insert  query
   $sendtodb = 'INSERT INTO `wp_postmeta`(`meta_id`, `post_id`, `meta_key`, `meta_value`)
    VALUES ("",'.$id.',"discounted_actions","'.$daterange.'"),
    ("",'.$id.',"deal_status","active")'; 
//   $sendtodb = 'INSERT INTO `wp_postmeta`(`meta_id`, `post_id`, `meta_key`, `meta_value`)
//   VALUES ("",'.$id.',"("discounted_actions","deal_status"),",("'.$daterange.'" , "active"))'; 


// ("'.$daterange.'" , 'true'),
$wpdb->query($sendtodb); //insert imported entries query
// echo 'query run';

$price = $product->get_price();
$getsaleprice = $product->get_sale_price();
$product_id = $product->get_id();
$product->set_sale_price($saleprice);
}
                   
            }
            } 
            
            ?>
              
            </form>
            <script>



let ids = '<?php echo $updatedIDs ?>';   
    //let text = "How are you doing today?";
const myArray = ids.split(",");
   if (history.pushState) {
        const newurl = window.location.href  + "&ids="+ myArray;
          
            // URLWithQueryString
        window.history.replaceState({ path: newurl }, "", newurl);
        } 


    jQuery(document).ready(function() {
    jQuery('#pickStartDate').datepicker({
        beforeShowDay: function(date){
          return [date.getDay()===3];
        }
    });
});
jQuery(document).ready(function() {
    jQuery('#pickEndDate').datepicker({
        beforeShowDay: function(date){
          return [date.getDay()===2];
        }
    });
});

</script>




            <?php
        }
    }

    /** Singleton instance */
    public static function get_instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}


add_action('woocommerce_update_product', 'updateSaleprice');


function updateSaleprice(){
    global $product_id;
    global $saleprice;
    update_post_meta($product_id, '_sale_price', $saleprice);
    
   
}

add_action('plugins_loaded', function () {
    Proposed_Products_Date::get_instance();
});






