<?php

/*
Plugin Name: Discounted Deals
Plugin URI: https://www.wpstairs.com/
Description: Demo on how WP_List_Table Class works
Version: 1.0
Author: Muhammad Umair Qureshi
Author URI:  https://www.umairqureshi.me
*/

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}


// testing comment
// new testing comment 
// again comment

class Master_Applications_List extends WP_List_Table
{   

    /** Class constructor */
    public function __construct()
    {

        parent::__construct([
            'singular' => __('Discounted Deals', 'sp'), //singular name of the listed records
            'plural' => __('Discounted Deals', 'sp'), //plural name of the listed records
            'ajax' => false //does this table support ajax?
        ]);

    }
}

require plugin_dir_path( __FILE__ ) .'proposed-product-dates.php';

class Master_Applications_Plugin
{


    // class instance
    static $instance;

    // class constructor
    public function __construct()
    {
        add_filter('set-screen-option', [__CLASS__, 'set_screen'], 10, 3);
        add_action('admin_menu', [$this, 'plugin_menu']);
    }

    public function plugin_menu()
    {

        $hook = add_menu_page(
            'Discounted Deals',
            'Discounted Deals',
            'manage_options',
            'discounted_deals',
            [$this, 'plugin_settings_page']
        );
    }

    public function plugin_settings_page()
    {
        global $wpdb;
        global $post;
        // $terms = get_the_terms( $post->ID, 'product_cat' );

        $entries_count = $wpdb->get_results ("SELECT * FROM `wp_posts` WHERE `post_type`= 'product'" );

        // inserts proposed interview times into database
        // if (isset($_POST['proposed_interview_times'])) {
        //     submitProposedInterviewsRecords();
        // }

        ?>


<div class="wrap">
    <br>
    <ul class =  main-tabs>
   
    <li  id  = "f-tab" class = "active"><a href="#tab-1"> Deals </a></li>
    <li id = "s-tab" class = "user-status-tab"><a href="#tab-2" id = "tab2"> Deals Log </a></li> 
    </ul>




     
<style>


.search-result{
  background-color: #e23356;
  font-size: 28px !important;
    font-family: serif;
    color: white;
    padding: 15px !important;
    text-align: center;
    /* width: 20%; */
}

.btn-continue{
  background-color: #0073aa;
    font-size: 22px;
    font-family: serif;
    color: white;
  
    padding-left: 30px;
    padding-right: 30px;
    padding-bottom: 10px;
    margin-bottom: 10px;
    border: 1px solid #0073aa;
    border-radius:5px;
   cursor:pointer;
    padding-top: 10px;
}

.btn-submit{
  background-color: #e23356;
    font-size: 20px;
    font-family: serif;
    color: white;
  
    padding-left: 30px;
    padding-right: 30px;
    padding-bottom: 8px;
    border: 1px solid #e23356;
    border-radius: 10px;
   cursor: pointer;
    padding-top: 8px;

}
.main-tabs {
	float: left;
	width: 100%;
	margin: 0;
	list-style-type: none;
	border-bottom: 1px solid transparent;
}

.main-tabs > li {
	float: left;
	margin-bottom: -1px;
	background-color: #eee;
}

.main-tabs > li > a {
	margin-right: 2px;
	line-height: 1.5;
	padding: 10px 35px;
    font-size: 16px;
	border: 1px solid transparent;
	border-radius: 4px 4px 0 0;
	float: left;
	text-decoration: none;
}

.main-tabs > li > a:hover {
	border-color: #eee #eee #ddd;
}


.main-tabs > li.active > a{
  background-color: #0073aa;
  color:white;
  /* font-size: 20px; */
    /* font-family: serif; */
}


.main-tabs > li.active > a:focus,
.main-tabs > li.active > a:hover,{
	color: #555;
	cursor: default;
	/*background-color: #eeeeee;*/
	background-color: #fff;
	border-color: transparent;
}

.tab-content > .tab-pane {
	float: left;
	width: 100%;
	display: none;
}

.tab-content > .tab-pane.active {
	display: block;
	padding: 10px;
	background-color: #fff;
	box-shadow: 0 5px 4px -2px rgba(0, 0, 0, 0.15);
}
#myTable td , #deals-log td{
    text-align: center;
}
#myTable .custom-pro-title , #deals-log .custom-pro-title{
    text-align: left;
}
</style>
 <!-- script for switch the tabs start -->
<script>
		window.addEventListener("load", function() {

	// store tabs variables
		var tabs = document.querySelectorAll("ul.main-tabs > li");
    //alert("tabs"+tabs);

			for (i = 0; i < tabs.length; i++) {
				tabs[i].addEventListener("click", switchTab);
				
	}

	function switchTab(event) {
		event.preventDefault();

		document.querySelector("ul.main-tabs li.active").classList.remove("active");
		document.querySelector(".tab-pane.active").classList.remove("active");

		var clickedTab = event.currentTarget;
		var anchor = event.target;
		var activePaneID = anchor.getAttribute("href");

		clickedTab.classList.add("active");
		document.querySelector(activePaneID).classList.add("active");
    //var myString = window.location.href.substring(window.location.href.lastIndexOf('id') + 1);
   // alert("myString"+myString);
		

	}

});

    </script>

<!-- End -->



        <?php


        echo '
        <!-- tab 1 start -->
<div class="tab-content">
       
        <div id=tab-1 class = "tab-pane active ">
    <br>
        <button id="propose-product-dates" type="button" class="btn btn-info btn-sm btn-continue"> Continue to next step </button>
        <br><br>
        <table class="cell-border tablepress dataTable" id="myTable">
        <thead>
        <tr>
        <th>ID</th>
        <th>Product Name</th>

      
        <th>Category Name</th>
        <th>Deals Date</th>
        <th>Select</th>
        </tr>
        </thead>';

        if ($entries_count) {
            foreach ($entries_count as $entry) {
                $metaEntries = $wpdb->get_row ("SELECT * FROM `wp_postmeta` WHERE post_id = '" . $entry->ID . "' AND meta_key= 'discounted_actions'  " );
        
                $product_cats = wp_get_post_terms( $entry->ID, 'product_cat' );


                // if(is_null($metaEntries)){
              
                ?> 
                <tr>
                    
                    <td><?php echo $entry->ID; ?></td> 
                    <td class = "custom-pro-title"><?php echo $entry->post_title; ?></td>
                    <!-- <td><?php //print_r($product_cats); ?></td> -->
                    <td><?php echo $product_cats[0]->name; ?></td>
                    <td><?php echo $metaEntries->meta_value; ?></td>
                 
                    <?php if($metaEntries->meta_value != ''){
                    
                    ?>
                    <td ><input style = "opacity: 0.5; pointer-events: none;" type="checkbox" name="ids" value="<?php echo $entry->ID ?>"/></td>
                    <?php }
                    else{?>

                    <td><input type="checkbox" name="ids" value="<?php echo $entry->ID ?>"/></td>
                    <?php }?>
                </tr>
                <?php
            // }
        }
        } else { ?>
            <tr>
                <td colspan="15" align="center"> No Data Found</td>
            </tr>
        <?php }
        echo '</table>
        </div> ';
        ?>
        <script type="text/javascript">
         jQuery(document).ready( function ()
          { 
            jQuery('#myTable').DataTable({
                
                    "pageLength": 25,
            });

            jQuery('#propose-product-dates').click(function () {
                var site_url = '<?php echo get_site_url(); ?>';
                    var ids = '';
                    jQuery.each(jQuery('input[name="ids"]:checked'), function () {
                        ids += jQuery(this).val() + ',';
                    });

                    ids = ids.replace(/,$/g, '');    // removing comma from last
                    // alert(ids);
                    window.location = site_url + '/wp-admin/admin.php?page=proposed_product_dates&ids=' + ids ;
                });

} );
			
		
        </script>
        <?php
        echo '
        <!-- tab 1 start -->

       
<div id= "tab-2" class = "tab-pane">
       
      
<br>
        <table class="cell-border tablepress dataTable" id="deals-log">
        <thead>
        <tr>
        <th>ID</th>
        <th>Product Name</th>

      
        <th>Category Name</th>
        <th>Deals Date</th>
     
        </tr>
        </thead>';

        if ($entries_count) {
            foreach ($entries_count as $entry) {
                $metaEntries = $wpdb->get_row ("SELECT * FROM `wp_postmeta` WHERE post_id = '" . $entry->ID . "' AND meta_key= 'discounted_actions' " );

        
                $product_cats = wp_get_post_terms( $entry->ID, 'product_cat' );

                // echo $metaEntries;
                if(!is_null($metaEntries)){
              
                ?> 
                <tr>
                    
                    <td><?php echo $entry->ID; ?></td>
                    <td class = "custom-pro-title"><?php echo $entry->post_title; ?></td>
                    <!-- <td><?php //print_r($product_cats); ?></td> -->
                    <td><?php echo $product_cats[0]->name; ?></td>
                    <td><?php echo $metaEntries->meta_value; ?></td>
                  
                   
                </tr>
                <?php
            }
        }
        } else { ?>
            <tr>
                <td colspan="15" align="center"> No Data Found</td>
            </tr>
        <?php }
        echo '</table>
        </div> ';
        ?>
        <script type="text/javascript">
         jQuery(document).ready( function ()
          { 
            jQuery('#deals-log').DataTable({
                
                    "pageLength": 10,
            });

            jQuery('#propose-product-dates').click(function () {
                var site_url = '<?php echo get_site_url(); ?>';
                    var ids = '';
                    jQuery.each(jQuery('input[name="ids"]:checked'), function () {
                        ids += jQuery(this).val() + ',';
                    });

                    ids = ids.replace(/,$/g, '');    // removing comma from last
                    // alert(ids);
                    window.location = site_url + '/wp-admin/admin.php?page=proposed_product_dates&ids=' + ids ;
                });

} );
			
		
        </script>

      

       

</div>

<?php
    }

    /** Singleton instance */
    public static function get_instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}

add_action('plugins_loaded', function () {
    Master_Applications_Plugin::get_instance();
});
